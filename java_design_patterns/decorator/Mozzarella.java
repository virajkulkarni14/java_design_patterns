package decorator;


/**
 * Write a description of class Mozzarella here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Mozzarella extends ToppingDecorator
{
    /**
     * Constructor for objects of class Mozzarella
     */
    public Mozzarella(Pizza newPizza)
    {
        // initialise instance variables
        super(newPizza);
        
        System.out.println("Adding Dough");
        
        System.out.println("Adding Mozzarella");
    }

    public String getDescription()
    {
        return tempPizza.getDescription() + ", Mozzarella";
    }
    
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y    a sample parameter for a method
     * @return        the sum of x and y 
     */
    public double getCost()
    {
        // put your code here
        return tempPizza.getCost() + 0.50;
    }
}

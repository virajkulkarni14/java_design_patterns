package decorator;


/**
 * Abstract class ToppingDecorator - write a description of the class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class ToppingDecorator implements Pizza
{
    // instance variables - replace the example below with your own
    protected Pizza tempPizza;
    
    public ToppingDecorator(Pizza newPizza)
    {
        tempPizza = newPizza;
    }
    
    public String getDescription()
    {
        return tempPizza.getDescription();
    }
    
    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y    a sample parameter for a method
     * @return        the sum of x and y 
     */
    public double getCost()
    {
        // put your code here
        return tempPizza.getCost();
    }
}

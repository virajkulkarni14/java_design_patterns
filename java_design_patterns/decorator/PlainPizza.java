package decorator;


/**
 * Write a description of class PlainPizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlainPizza implements Pizza
{
    // instance variables - replace the example below with your own
    
    /**
     * Constructor for objects of class PlainPizza
     */
    public PlainPizza()
    {
        // initialise instance variables
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public String getDescription()
    {
        return "Thin Dough";
    }
    public double getCost()
    {
        // put your code here
        return 4.00;
    }
}

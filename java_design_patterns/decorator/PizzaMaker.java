package decorator;


/**
 * Write a description of class PizzaMaker here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PizzaMaker
{


    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public static void main(String[] args)
    {
        Pizza basicPizza = new TomatoSauce(new Mozzarella(new PlainPizza()));
        
        System.out.println("Ingredients: " + basicPizza.getDescription());
        
        System.out.println("Price: " + basicPizza.getCost());
    }
}

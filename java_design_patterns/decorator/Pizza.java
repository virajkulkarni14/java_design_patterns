package decorator;


/**
 * Write a description of interface Pizza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public interface Pizza
{
    /**
     * An example of a method header - replace this comment with your own
     * 
     * @param  y    a sample parameter for a method
     * @return        the result produced by sampleMethod 
     */
    public String getDescription();
    
    public double getCost();
}

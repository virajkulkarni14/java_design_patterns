import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
package singleton;


/**
 * Write a description of class Singleton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Singleton
{
    // instance variables - replace the example below with your own
    private static Singleton firstInstance = null;

    /**
     * Constructor for objects of class Singleton
     */
    private Singleton()
    {
        // initialise instance variables
        public static Singleton getInstance() {
            if(firstInstance == null) {
                firstInstance = new Singleton();
            }
        
            return firstInstance;
        }
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public int sampleMethod(int y)
    {
        // put your code here
        return x + y;
    }
}
